<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PersonRepository")
 */
class Person
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $familyName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $givenName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $additionalName;

    /**
     * @ORM\Column(type="object")
     */
    private $address;

    /**
     * @ORM\Column(type="object", nullable=true)
     */
    private $alumniOf;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Application", mappedBy="Person", orphanRemoval=true)
     */
    private $applications;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isReviewer;

    public function __construct()
    {
        $this->applications = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getFamilyName(): ?string
    {
        return $this->familyName;
    }

    public function setFamilyName(string $familyName): self
    {
        $this->familyName = $familyName;

        return $this;
    }

    public function getGivenName(): ?string
    {
        return $this->givenName;
    }

    public function setGivenName(string $givenName): self
    {
        $this->givenName = $givenName;

        return $this;
    }

    public function getAdditionalName(): ?string
    {
        return $this->additionalName;
    }

    public function setAdditionalName(string $additionalName): self
    {
        $this->additionalName = $additionalName;

        return $this;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setAddress($address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getAlumniOf()
    {
        return $this->alumniOf;
    }

    public function setAlumniOf($alumniOf): self
    {
        $this->alumniOf = $alumniOf;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Collection|Application[]
     */
    public function getApplications(): Collection
    {
        return $this->applications;
    }

    public function addApplication(Application $application): self
    {
        if (!$this->applications->contains($application)) {
            $this->applications[] = $application;
            $application->setPerson($this);
        }

        return $this;
    }

    public function removeApplication(Application $application): self
    {
        if ($this->applications->contains($application)) {
            $this->applications->removeElement($application);
            // set the owning side to null (unless already changed)
            if ($application->getPerson() === $this) {
                $application->setPerson(null);
            }
        }

        return $this;
    }

    public function getIsReviewer(): ?bool
    {
        return $this->isReviewer;
    }

    public function setIsReviewer(bool $isReviewer): self
    {
        $this->isReviewer = $isReviewer;

        return $this;
    }
}
