<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ApplicationRepository")
 */
class Application
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Person", inversedBy="applications")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Person;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $degreeHeld;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $degreeSought;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $gradDate;

    /**
     * @ORM\Column(type="text")
     */
    private $shortTerm;

    /**
     * @ORM\Column(type="text")
     */
    private $longTerm;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tag", mappedBy="Application")
     */
    private $tags;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isDraft;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isSpam;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getPerson(): ?Person
    {
        return $this->Person;
    }

    public function setPerson(?Person $Person): self
    {
        $this->Person = $Person;

        return $this;
    }

    public function getDegreeHeld(): ?array
    {
        return $this->degreeHeld;
    }

    public function setDegreeHeld(?array $degreeHeld): self
    {
        $this->degreeHeld = $degreeHeld;

        return $this;
    }

    public function getDegreeSought(): ?array
    {
        return $this->degreeSought;
    }

    public function setDegreeSought(?array $degreeSought): self
    {
        $this->degreeSought = $degreeSought;

        return $this;
    }

    public function getGradDate(): ?\DateTimeInterface
    {
        return $this->gradDate;
    }

    public function setGradDate(?\DateTimeInterface $gradDate): self
    {
        $this->gradDate = $gradDate;

        return $this;
    }

    public function getShortTerm(): ?string
    {
        return $this->shortTerm;
    }

    public function setShortTerm(string $shortTerm): self
    {
        $this->shortTerm = $shortTerm;

        return $this;
    }

    public function getLongTerm(): ?string
    {
        return $this->longTerm;
    }

    public function setLongTerm(string $longTerm): self
    {
        $this->longTerm = $longTerm;

        return $this;
    }

    /**
     * @return Collection|Tag[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
            $tag->setApplication($this);
        }

        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
            // set the owning side to null (unless already changed)
            if ($tag->getApplication() === $this) {
                $tag->setApplication(null);
            }
        }

        return $this;
    }

    public function getIsDraft(): ?bool
    {
        return $this->isDraft;
    }

    public function setIsDraft(bool $isDraft): self
    {
        $this->isDraft = $isDraft;

        return $this;
    }

    public function getIsSpam(): ?bool
    {
        return $this->isSpam;
    }

    public function setIsSpam(bool $isSpam): self
    {
        $this->isSpam = $isSpam;

        return $this;
    }
}
