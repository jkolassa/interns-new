<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TagRepository")
 */
class Tag
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Application", inversedBy="tags")
     */
    private $Application;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TagType", inversedBy="tag")
     */
    private $tagType;

    public function getId()
    {
        return $this->id;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getApplication(): ?Application
    {
        return $this->Application;
    }

    public function setApplication(?Application $Application): self
    {
        $this->Application = $Application;

        return $this;
    }

    public function getTagType(): ?TagType
    {
        return $this->tagType;
    }

    public function setTagType(?TagType $tagType): self
    {
        $this->tagType = $tagType;

        return $this;
    }
}
