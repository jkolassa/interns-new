<?php

namespace App\Repository;

use App\Entity\EducationalOrganization;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EducationalOrganization|null find($id, $lockMode = null, $lockVersion = null)
 * @method EducationalOrganization|null findOneBy(array $criteria, array $orderBy = null)
 * @method EducationalOrganization[]    findAll()
 * @method EducationalOrganization[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EducationalOrganizationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EducationalOrganization::class);
    }

//    /**
//     * @return EducationalOrganization[] Returns an array of EducationalOrganization objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EducationalOrganization
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
