<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Encoder\DecoderInterface;
use Symfony\Component\Serializer\Exception\RuntimeException;

/**
 * Class AppFixturesBase
 * @package App\DataFixtures
 */
abstract class AbstractAppFixture extends Fixture
{
    /**
     * @var CsvEncoder
     */
    protected $decoder;

    /**
     * @var string
     */
    protected $appPath;

    /**
     * AbstractAppFixtures constructor.
     * @param DecoderInterface $decoder
     * @param string $appPath
     * @throws RuntimeException
     * @throws InvalidArgumentException
     */
    public function __construct(DecoderInterface $decoder, string $appPath)
    {
        $this->decoder = $decoder;
        $this->appPath = $appPath;
    }

    /**
     * @param string $fileName
     * @param string $type
     * @return string
     */
    public function getAppPath(string $fileName, string $type = 'csv'): string
    {
        return $this->appPath.'/Seeds/'.$fileName.'.'.$type;
    }
}