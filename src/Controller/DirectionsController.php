<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DirectionsController extends Controller
{
    /**
     * @Route("/directions", name="directions")
     */
    public function index()
    {
        return $this->render('directions/index.html.twig', [
            'controller_name' => 'DirectionsController',
        ]);
    }
}
