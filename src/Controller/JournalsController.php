<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class JournalsController extends Controller
{
    /**
     * @Route("/journals", name="journals")
     */
    public function index()
    {
        return $this->render('journals/index.html.twig', [
            'controller_name' => 'JournalsController',
        ]);
    }
}
