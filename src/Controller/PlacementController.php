<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PlacementController extends Controller
{
    /**
     * @Route("/placement", name="placement")
     */
    public function index()
    {
        return $this->render('placement/index.html.twig', [
            'controller_name' => 'PlacementController',
        ]);
    }
}
